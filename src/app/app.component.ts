import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angularopenclassoom 2';

  posts = [
    {
      title: "Premier post",
      content: "Article super détaillé. Lofhjsfs sd df fasdhfgsjkhfg kjahf f jfh sjaf asjfldshhfsajhf sdkf sadkfédskaljf éskdfj adskfj askfkjéaskfj sdkfj sdélklfj sdlékfj dslé j",
      loveIts: 0,
      created_at: new Date()
    },
    {
      title: "Deuxieme post",
      content: "Article super intéressant",
      loveIts: 0,
      created_at: new Date()
    },
    {
      title: "Troisème post",
      content: "Article totalement nul",
      loveIts: 0,
      created_at: new Date()
    },
  ]


  constructor() {
  }

}
